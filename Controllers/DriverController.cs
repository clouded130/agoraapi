using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using AgoraApi.Models;

namespace AgoraApi.Controllers
{
    [Route("api/driver")]
    [ApiController]
    public class DriverController : ControllerBase
    {
        private readonly DriverContext _context;

        public DriverController(DriverContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<List<Driver>> GetAll()
        {
            var test = _context.Driver.ToList();
            return _context.Driver.ToList();
        }

        [HttpGet("{id}", Name = "GetDriver")]
        public ActionResult<Driver> GetById(long id)
        {
            var item = _context.Driver.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Create(Driver item)
        {
            _context.Driver.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetDriver", new { id = item.Id }, item);
        }


        [HttpPut("{id}")]
        public IActionResult Update(long id, Driver item)
        {
            var driver = _context.Driver.Find(id);
            if (driver == null)
            {
                return NotFound();
            }
            driver.Name = item.Name;
            driver.Mobile = item.Mobile;
            driver.Address = item.Address;
            driver.Email = item.Email;

            _context.Driver.Update(driver);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var todo = _context.Driver.Find(id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Driver.Remove(todo);
            _context.SaveChanges();
            return NoContent();
        }
    }
}