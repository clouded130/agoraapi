namespace AgoraApi
{
    public class Device
    {
        public long Id { get; set; }
        public int version { get; set; }
        public string Device_Name { get; set; }
    }
}