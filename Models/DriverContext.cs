using Microsoft.EntityFrameworkCore;

namespace AgoraApi.Models
{
    public class DriverContext : DbContext
    {
        public DriverContext(DbContextOptions<DriverContext> options)
            : base(options)
        {
        }

        public DbSet<Driver> Driver { get; set; }
        public DbSet<Device> Device { get; set; }
    }
}